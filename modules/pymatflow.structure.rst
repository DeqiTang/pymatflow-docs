pymatflow.structure package
===========================

Submodules
----------

pymatflow.structure.crystal module
----------------------------------

.. automodule:: pymatflow.structure.crystal
   :members:
   :undoc-members:
   :show-inheritance:

pymatflow.structure.molecule module
-----------------------------------

.. automodule:: pymatflow.structure.molecule
   :members:
   :undoc-members:
   :show-inheritance:

pymatflow.structure.neb module
------------------------------

.. automodule:: pymatflow.structure.neb
   :members:
   :undoc-members:
   :show-inheritance:

pymatflow.structure.tools module
--------------------------------

.. automodule:: pymatflow.structure.tools
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pymatflow.structure
   :members:
   :undoc-members:
   :show-inheritance:
